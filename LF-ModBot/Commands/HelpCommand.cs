﻿using Discord;
using Discord.Commands;
using LFModBot.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFModBot.Commands
{
    public class HelpCommand : ModuleBase<SocketCommandContext>
    {
        // Show help
        [Command("help")]
        [Summary("Displays command guide")]
        public async Task HelpAsync()
        {
            if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
            {
                char prefix = Globals.CurrentGuild(Context).Prefix;
                StringBuilder general = new StringBuilder(1024);
                general.AppendLine($"Example: {prefix}help");
                general.AppendLine($"{prefix}help - Shows this dialog");
                general.AppendLine($"{prefix}ping - pong! (tests if the bot's responding)");
                general.AppendLine("Notes:");
                general.AppendLine("1. IRole types accept the role ID or `@mentioning` the role");
                general.AppendLine("2. IChannel types accept `#name`, `<#ID>` or their ID");
                general.AppendLine("3. IUser types accept `@name`, name#discrim, and their ID");

                StringBuilder categories = new StringBuilder(1024);
                categories.AppendLine($"For additional information on the following categories, run {prefix}category help|?");
                categories.AppendLine($"{prefix}guild - Used to edit this guild's configuration");
                categories.AppendLine($"{prefix}mod - Moderation commands");

                


                EmbedBuilder embed = new EmbedBuilder
                {
                    Color = Color.Green,
                    ThumbnailUrl = "https://www.luzfaltex.com/media/7-sudo-icon-png/",
                    Title = $"{Globals.MetaData.ProductName} version {Globals.MetaData.FileVersion}",
                    Description = Globals.MetaData.FileDescription + $" Copyright {Globals.GetYear()} LuzFaltex, all rights reserved"
                };
                embed.AddField("Command Information", $"Command prefix: `{Globals.CurrentConfig.GetGuild(Context.Guild.Id).Prefix}`\r\nArgumentSyntax: [optional] <required>");
                embed.AddField("General Commands", general.ToString());
                embed.AddField("Command Categories", categories.ToString());

                await ReplyAsync("", embed: embed.Build());
            }
        }

    }
}
