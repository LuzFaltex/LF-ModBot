﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LFModBot.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFModBot.Commands
{
    [Group("mod")]
    public class ModerationCommands : ModuleBase<SocketCommandContext>
    {
        [Command("help")]
        [Alias("?")]
        public async Task GetHelp()
        {
            char prefix = Globals.CurrentGuild(Context).Prefix;
            StringBuilder mod = new StringBuilder(1024);
            mod.AppendLine($"Example: {prefix}ban Foxtrek_64#3858 2020-05-01 15:30:00Z");
            mod.AppendLine();
            mod.AppendLine($"{prefix}mod Kick <IUser> [reason] - Forcefully removes a user from the server");
            mod.AppendLine($"{prefix}mod Ban <IUser> [reason] - Kicks and bans a user until the specified date, or permanently if no date is specified. YYYY-MM-DD hh:mm:ssZ (24 hour, UTC)");
            mod.AppendLine($"{prefix}mod Bonk <IUser> <UserNameAndDiscrim]> [reason] - Bans a user who is not on the server until the specified date or permanently. YYYY-MM-DD hh:mm:ssZ (24 hour, UTC)");
            mod.AppendLine($"{prefix}mod Unban <IUser> - Unbans a user.");
            mod.AppendLine($"{prefix}mod Modify <int CaseID> [reason] - Modifies an existing ban to add, change, or remove a reason. Use \"\" to remove the reason.");

            EmbedBuilder embed = new EmbedBuilder
            {
                Color = Color.Green,
                ThumbnailUrl = "https://www.luzfaltex.com/media/7-sudo-icon-png/",
                Title = $"{Globals.MetaData.ProductName} version {Globals.MetaData.FileVersion}",
                Description = Globals.MetaData.FileDescription + $" Copyright {Globals.GetYear()} LuzFaltex, all rights reserved"
            };
            embed.AddField("Command Information", $"Command prefix: `{Globals.CurrentConfig.GetGuild(Context.Guild.Id).Prefix}`\r\nArgumentSyntax: [optional] <required>");
            embed.AddField("Moderation Commands", mod.ToString());

            await ReplyAsync("", embed: embed.Build());
        }

        [Command("bonk")]
        public async Task BonkUser(ulong userID, string UserNameAndDiscrim, [Remainder] string reason = "")
        {
            if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
            {
                if (Globals.UserHasPermission(Context, Globals.RequiredRole.Mod))
                {
                    EmbedBuilder errorEmbed = new EmbedBuilder
                    {
                        Color = Color.Red
                    };

                    if (Context.User.Id == userID)
                    {
                        errorEmbed.Description = "You cannot ban yourself.";
                        await ReplyAsync("", embed: errorEmbed.Build());
                    }
                    if (userID == Context.Guild.Owner.Id)
                    {
                        errorEmbed.Description = "You cannot ban the owner.";
                        await ReplyAsync("", embed: errorEmbed.Build());
                    }

                    errorEmbed.Title = "🔨";
                    Guild guild = Globals.CurrentGuild(Context);

                    Globals.CurrentConfig.Guilds.Remove(guild);

                    ModEvent me = new ModEvent("Bonk", guild.ModEvents.Count, UserNameAndDiscrim.Split('#')[0], UserNameAndDiscrim.Split('#')[1], userID, reason, Context.User.Username, Context.User.Discriminator, Context.User.Id, DateTime.Today);
                    errorEmbed.Description = me.ToString();

                    if (Context.Guild.GetChannel(guild.ModLogChannel) == null)
                    {
                        await ReplyAsync("", embed: errorEmbed.Build());
                    }
                    else
                    {
                        IUserMessage msg = await ((ITextChannel)Context.Guild.GetChannel(guild.ModLogChannel)).SendMessageAsync("", embed: errorEmbed.Build());
                        me.AddMessageID(msg.Id);
                    }
                    guild.ModEvents.Add(me);

                    Globals.CurrentConfig.Guilds.Add(guild);
                    Globals.CurrentConfig.WriteChanges();

                    if (string.IsNullOrEmpty(reason))
                    {
                        await ReplyAsync($"{Context.User.Mention} Please run {Globals.CurrentGuild(Context).Prefix}mod modify {me.CaseID} \"Your Reason...\" to add a reason for banning this user.");
                    }
                    // await user.KickAsync(reason, new RequestOptions() { AuditLogReason = reason });

                }
            }
        }

        [Command("ban")]
        public async Task BanUser(SocketGuildUser user, string reason = "")
        {
            if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
            {
                if (Globals.UserHasPermission(Context, Globals.RequiredRole.Mod))
                {
                    EmbedBuilder errorEmbed = new EmbedBuilder
                    {
                        Color = Color.Red
                    };

                    if (Context.User.Id == user.Id)
                    {
                        errorEmbed.Description = "You cannot ban yourself.";
                        await ReplyAsync("", embed: errorEmbed.Build());
                    }
                    if (Context.User == Context.Guild.Owner)
                    {
                        errorEmbed.Description = "You cannot ban the owner.";
                        await ReplyAsync("", embed: errorEmbed.Build());
                    }

                    errorEmbed.Title = "🔨";
                    Guild guild = Globals.CurrentGuild(Context);
                    Globals.CurrentConfig.Guilds.Remove(guild);
                    ModEvent me = new ModEvent("Ban", guild.ModEvents.Count, user.Username, user.Discriminator, user.Id, reason, Context.User.Username, Context.User.Discriminator, Context.User.Id, DateTime.Now);
                    errorEmbed.Description = me.ToString();

                    if (Context.Guild.GetChannel(guild.ModLogChannel) == null)
                    {
                        await ReplyAsync("", embed: errorEmbed.Build());
                    }
                    else
                    {
                        IUserMessage msg = await ((ITextChannel)Context.Guild.GetChannel(guild.ModLogChannel)).SendMessageAsync("", embed: errorEmbed.Build());
                        me.AddMessageID(msg.Id);
                    }
                    guild.ModEvents.Add(me);
                    Globals.CurrentConfig.Guilds.Add(guild);
                    Globals.CurrentConfig.WriteChanges();

                    if (string.IsNullOrEmpty(reason))
                    {
                        await ReplyAsync($"{Context.User.Mention} Please run {Globals.CurrentGuild(Context).Prefix}mod modify {me.CaseID} \"Your Reason...\" to add a reason for banning this user.");
                    }

                    await Context.Guild.AddBanAsync(user, 0, reason, new RequestOptions() { AuditLogReason = reason });
                }
            }
        }

        [Command("kick")]
        public async Task KickUser(IGuildUser user, string reason = "")
        {
            if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
            {
                if (Globals.UserHasPermission(Context, Globals.RequiredRole.Mod))
                {
                    EmbedBuilder errorEmbed = new EmbedBuilder
                    {
                        Color = Color.Red
                    };

                    if (Context.User.Id == user.Id)
                    {
                        errorEmbed.Description = "You cannot kick yourself.";
                        await ReplyAsync("", embed: errorEmbed.Build());
                    }
                    if (Context.User == Context.Guild.Owner)
                    {
                        errorEmbed.Description = "You cannot kick the owner.";
                        await ReplyAsync("", embed: errorEmbed.Build());
                    }

                    errorEmbed.Title = "👢";
                    Guild guild = Globals.CurrentGuild(Context);

                    Globals.CurrentConfig.Guilds.Remove(guild);

                    ModEvent me = new ModEvent("Kick", guild.ModEvents.Count, user.Username, user.Discriminator, user.Id, reason, Context.User.Username, Context.User.Discriminator, Context.User.Id, DateTime.Today);
                    errorEmbed.Description = me.ToString();


                    if (Context.Guild.GetChannel(guild.ModLogChannel) == null)
                    {
                        await ReplyAsync("", embed: errorEmbed.Build());
                    }
                    else
                    {
                        IUserMessage msg = await ((ITextChannel)Context.Guild.GetChannel(guild.ModLogChannel)).SendMessageAsync("", embed: errorEmbed.Build());
                        me.AddMessageID(msg.Id);
                    }
                    guild.ModEvents.Add(me);
                    Globals.CurrentConfig.Guilds.Add(guild);
                    Globals.CurrentConfig.WriteChanges();

                    if (string.IsNullOrEmpty(reason))
                    {
                        await ReplyAsync($"{Context.User.Mention} Please run {Globals.CurrentGuild(Context).Prefix}mod modify {me.CaseID} \"Your Reason...\" to add a reason for kicking this user.");
                    }

                    await user.KickAsync(reason, new RequestOptions() { AuditLogReason = reason });
                }
            }
        }

        [Command("unban")]
        public async Task UnbanUser(IGuildUser user)
        {
            if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
            {
                if (Globals.UserHasPermission(Context, Globals.RequiredRole.Mod))
                {
                    EmbedBuilder messageEmbed = new EmbedBuilder();                    

                    int removed = 0;
                    Guild guild = Globals.CurrentGuild(Context);
                    Globals.CurrentConfig.Guilds.Remove(guild);
                    try
                    {
                        
                        foreach (ModEvent me in guild.ModEvents.Where(x => x.Type == "Ban" || x.Type == "Bonk"))
                        {
                            var message = await Context.Guild.GetTextChannel(Globals.CurrentGuild(Context).ModLogChannel).GetMessageAsync(me.MessageID);
                            await message.DeleteAsync();
                            guild.ModEvents.Remove(me);
                            removed++;
                        }
                        messageEmbed.Description = $"✓ {user.Mention} unbanned successfully.";
                        messageEmbed.Color = Color.Green;
                    }
                    catch (Exception ex)
                    {
                        if (ex is ArgumentNullException)
                        {
                            messageEmbed.Description = $"😕 Could not find the user to unban. Did you type their name correctly?";
                            messageEmbed.Color = Color.Orange;
                        }
                        else
                        {
                            messageEmbed.Description = $"Error: {ex.Message}";
                            messageEmbed.Color = Color.Red;
                        }
                    }
                    finally
                    {
                        Globals.CurrentConfig.Guilds.Add(guild);
                        Globals.CurrentConfig.WriteChanges();
                        await ReplyAsync("", embed: messageEmbed.Build());
                    }
                }
            }
        }

        [Command("modify")]
        public async Task Modify(int caseID, string reason = "")
        {
            if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
            {
                if (Globals.UserHasPermission(Context, Globals.RequiredRole.Mod))
                {
                    Guild guild = Globals.CurrentGuild(Context);
                    Globals.CurrentConfig.Guilds.Remove(guild);
                    EmbedBuilder messageEmbed = new EmbedBuilder();
                    ModEvent me = guild.ModEvents.FirstOrDefault(x => x.CaseID == caseID);

                    if (me != null)
                    {
                        messageEmbed.Description = $"✓ Mod Report reason updated from {me.Reason} to {reason}.";
                        messageEmbed.Color = Color.Green;
                        guild.ModEvents.Remove(me);
                        me.Reason = reason;
                        guild.ModEvents.Add(me);
                    }
                    else
                    {
                        messageEmbed.Description = $"😕 Could not find the Mod Report to update. Did you type the ID correctly?";
                        messageEmbed.Color = Color.Orange;
                    }

                    Globals.CurrentConfig.Guilds.Add(guild);
                    Globals.CurrentConfig.WriteChanges();
                    await ReplyAsync("", embed: messageEmbed.Build());
                }
            }
        }
    }
}
