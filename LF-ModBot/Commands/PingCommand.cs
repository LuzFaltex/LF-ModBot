﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LFModBot.Commands
{
    public class PingCommand : ModuleBase<SocketCommandContext>
    {
        [Command("ping")]
        [Summary("pong")]
        public async Task PingPongAsync()
        {
            if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
            {
                // I accept no arguments
                await ReplyAsync("pong!");
            }
        }
    }
}
