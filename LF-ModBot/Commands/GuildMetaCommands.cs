﻿using Discord;
using Discord.Commands;
using LFModBot.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFModBot.Commands
{
    [Group("guild")]
    public class GuildMetaCommands : ModuleBase<SocketCommandContext>
    {
        [Command("info")]
        public async Task InfoCommand()
        {
            if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
            {
                Guild guild = Globals.CurrentConfig.GetGuild(Context.Guild.Id);
                EmbedBuilder embed = new EmbedBuilder();
                StringBuilder sb = new StringBuilder();
                sb.AppendLine($"Name: {guild.Name}");
                sb.AppendLine($"ID: {guild.ID}");
                sb.AppendLine($"Owner: {Context.Guild.GetUser(guild.Owner).Username}#{Context.Guild.GetUser(guild.Owner).Discriminator} ({guild.Owner})");
                sb.AppendLine($"Prefix: {guild.Prefix}");
                sb.AppendLine($"Admin Role: `{Globals.GetRoleText(Context, Globals.RequiredRole.Admin)}`");
                sb.AppendLine($"Mod Role: `{Globals.GetRoleText(Context, Globals.RequiredRole.Mod)}`");
                sb.AppendLine($"Mod Log Channel: <#{guild.ModLogChannel}>");
                sb.AppendLine($"Command Channels: {string.Join(", ", guild.CommandChannels.Select(x => $"<#{x}>"))}");
                sb.AppendLine($"Roles: {string.Join(", ", Context.Guild.Roles.Select(x => $"`{x.Name} ({x.Id})`"))}");
                /*sb.AppendLine("Banned Users:");
                foreach (ModEvent me in guild.ModEvents.Take(10))
                {
                    sb.AppendLine($" - User {me.User.Username}#{me.User.Discriminator} ({me.User.Id}) banned {(me.UnbanDate == DateTime.MaxValue ? "forever" : $"until { me.UnbanDate.ToString()}")}");
                }*/
                embed.Title = guild.Name;
                embed.ThumbnailUrl = Context.Guild.IconUrl;
                embed.Color = Color.Blue;
                embed.AddField("Guild Information", sb.ToString());

                await ReplyAsync("", embed: embed.Build());
            }
            
        }
        [Command("register")]
        public async Task RegisterCommand()
        {
            if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
            {
                if (Globals.UserHasPermission(Context, Globals.RequiredRole.Admin))
                {
                    Globals.RegisterGuild(Context.Guild);
                    await ReplyAsync("👍");
                }
            }
        }
        [Command("help")]
        [Alias("?")]
        public async Task GetHelp()
        {
            StringBuilder guild = new StringBuilder(1024);
            guild.AppendLine($"Example: {Globals.CurrentGuild(Context).Prefix}guild set Prefix .");
            guild.AppendLine(" - guild info - shows information about the current guild");
            guild.AppendLine(" - guild register - Updates or resets all guild information");
            guild.AppendLine();
            guild.AppendLine(" - guild set Prefix <char prefix> - Updates this server's prefix.");
            guild.AppendLine(" - guild set ModRole <IRole role> - Updates this server's mod role.");
            guild.AppendLine(" - guild set AdminRole <IRole role> - Updates this server's admin role.");
            guild.AppendLine(" - guild set ModLogChannel <IChannel channel> - Updates the moderation log channel.");
            guild.AppendLine();
            guild.AppendLine(" - guild get Name - returns the name of the guild");
            guild.AppendLine(" - guild get ID - returns the ID of the guild");
            guild.AppendLine(" - guild get Owner - returns the guild owner's name, discrim, and ID");
            guild.AppendLine(" - guild get Prefix - returns this bot's guild prefix");
            guild.AppendLine(" - guild get ModRole - returns the mod permission role");
            guild.AppendLine(" - guild get AdminRole - returns the admin permission role");
            guild.AppendLine(" - guild get ModLogChannel - returns this guild's Log Channel ID");
            guild.AppendLine(" - guild get CommandChannels - returns a list of Command Channels the channels this bot listens");
            guild.AppendLine(" - guild get BannedUsers - returns a list banned users");

            StringBuilder guild2 = new StringBuilder();
            guild2.AppendLine(" - guild add CommandChannel <IChannel channel> - Adds a channel which the bot listens to");
            guild2.AppendLine();
            guild2.AppendLine(" - guild remove CommandChannel <IChannel channel> - removes the specified channel");

            EmbedBuilder embed = new EmbedBuilder
            {
                Color = Color.Green,
                ThumbnailUrl = "https://www.luzfaltex.com/media/7-sudo-icon-png/",
                Title = $"{Globals.MetaData.ProductName} version {Globals.MetaData.FileVersion}",
                Description = Globals.MetaData.FileDescription + $" Copyright {Globals.GetYear()} LuzFaltex, all rights reserved"
            };
            embed.AddField("Command Information", $"Command prefix: `{Globals.CurrentConfig.GetGuild(Context.Guild.Id).Prefix}`\r\nArgumentSyntax: [optional] <required>");
            embed.AddField("Guild Commands", guild.ToString());
            embed.AddField("", guild2.ToString());

            await ReplyAsync("", embed: embed.Build());
        }

        [Group("get")]
        public class Get : ModuleBase<SocketCommandContext>
        {
            [Command("name")]
            public async Task GetName()
            {
                if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
                {
                    Guild guild = Globals.CurrentConfig.GetGuild(Context.Guild.Id);
                    await ReplyAsync($"Guild name: {guild.Name}");
                }
            }
            [Command("id")]
            public async Task GetId()
            {
                if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
                {
                    Guild guild = Globals.CurrentConfig.GetGuild(Context.Guild.Id);
                    await ReplyAsync($"Guild ID: {guild.ID}");
                }
            }
            [Command("owner")]
            public async Task GetOwner()
            {
                if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
                {
                    Guild guild = Globals.CurrentConfig.GetGuild(Context.Guild.Id);
                    await ReplyAsync($"Guild owner: {Context.Guild.GetUser(guild.Owner).Username}#{Context.Guild.GetUser(guild.Owner).Discriminator} ({guild.Owner})");
                }
            }
            [Command("prefix")]
            public async Task GetPrefix()
            {
                if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
                {
                    Guild guild = Globals.CurrentConfig.GetGuild(Context.Guild.Id);
                    await ReplyAsync($"Guild prefix: {guild.Prefix}");
                }
            }
            [Command("adminrole")]
            public async Task GetAdminRole()
            {
                if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
                {
                    await ReplyAsync($"Guild Admin Role ID: {Globals.GetRoleText(Context, Globals.RequiredRole.Admin)}");
                }
            }
            [Command("modrole")]
            public async Task GetModRole()
            {
                if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
                {
                    await ReplyAsync($"Guild Mod Role ID: {Globals.GetRoleText(Context, Globals.RequiredRole.Mod)}");
                }
            }
            [Command("modlogchannel")]
            public async Task GetModLogChannel()
            {
                if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
                {
                    Guild guild = Globals.CurrentConfig.GetGuild(Context.Guild.Id);
                    await ReplyAsync($"Guild Log Channel: {guild.ModLogChannel}");
                }
            }
            [Command("commandchannels")]
            public async Task GetCommandChannels()
            {
                if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
                {
                    Guild guild = Globals.CurrentConfig.GetGuild(Context.Guild.Id);
                    await ReplyAsync($"Guild Command Channels: {string.Join(",", guild.CommandChannels.Select(x => $"<#{x}>"))}");
                }
            }
            /*[Command("bannedusers")]
            public async Task GetBannedUsers()
            {
                if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
                {
                    Guild guild = Globals.CurrentConfig.GetGuild(Context.Guild.Id);
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Banned Users:");
                    foreach (ModEvent me in guild.ModEvents)
                    {
                        sb.AppendLine($" - User {me.User.Username}#{me.User.Discriminator} ({me.User.Id}) banned {(me.UnbanDate == DateTime.MaxValue ? "forever" : $"until { me.UnbanDate.ToString()}")}");
                    }
                    await ReplyAsync($"Banned: {guild.Prefix}");
                }
            }*/
        }
        [Group("set")]
        public class Set : ModuleBase<SocketCommandContext>
        {
            [Command("prefix")]
            public async Task SetPrefix(char prefix)
            {
                if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
                {
                    if (Globals.UserHasPermission(Context, Globals.RequiredRole.Admin))
                    {
                        Guild guild = Globals.CurrentConfig.GetGuild(Context.Guild.Id);
                        string result = $"Guild prefix changed from `{guild.Prefix}` to `{prefix}`.";
                        Globals.CurrentConfig.Guilds.Remove(guild);
                        guild.Prefix = prefix;
                        Globals.CurrentConfig.Guilds.Add(guild);
                        Globals.CurrentConfig.WriteChanges();
                        await ReplyAsync($"Guild updated: {result}");
                    }
                }
            }
            [Command("adminrole")]
            public async Task SetAdminRole(IRole role)
            {
                if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
                {
                    if (Globals.UserHasPermission(Context, Globals.RequiredRole.Admin))
                    {
                        Guild guild = Globals.CurrentGuild(Context);
                        IRole origRole = Context.Guild.GetRole(guild.AdminRole);
                        string result = $"Guild Admin Role changed from `{(origRole == null ? "null" : origRole.Mention)}` to `{role.Mention}`.";
                        Globals.CurrentConfig.Guilds.Remove(guild);
                        guild.AdminRole = role.Id;
                        Globals.CurrentConfig.Guilds.Add(guild);
                        Globals.CurrentConfig.WriteChanges();
                        await ReplyAsync($"Guild updated: {result}");
                    }
                }
            }
            [Command("modrole")]
            public async Task SetModRole(IRole role)
            {
                if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
                {
                    if (Globals.UserHasPermission(Context, Globals.RequiredRole.Admin))
                    {
                        Guild guild = Globals.CurrentGuild(Context);
                        IRole origRole = Globals.GetRole(Context, Globals.RequiredRole.Mod);
                        string result = $"Guild Mod Role changed from `{(origRole == null ? "null" : origRole.Mention)}` to `{role.Mention}`.";
                        Globals.CurrentConfig.Guilds.Remove(guild);
                        guild.ModRole = role.Id;
                        Globals.CurrentConfig.Guilds.Add(guild);
                        Globals.CurrentConfig.WriteChanges();
                        await ReplyAsync($"Guild updated: {result}");
                    }
                }
            }
            [Command("modlogchannel")]
            public async Task SetModLogChannel(IChannel channel)
            {
                if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
                {
                    if (Globals.UserHasPermission(Context, Globals.RequiredRole.Admin))
                    {
                        Guild guild = Globals.CurrentGuild(Context);
                        string result = $"Guild Log Channel changed from <#{guild.ModLogChannel}> to <#{channel.Id}>.";
                        Globals.CurrentConfig.Guilds.Remove(guild);
                        guild.ModLogChannel = channel.Id;
                        Globals.CurrentConfig.Guilds.Add(guild);
                        Globals.CurrentConfig.WriteChanges();
                        await ReplyAsync($"Guild updated: {result}");
                    }
                }
            }
        }
        [Group("add")]
        public class Add : ModuleBase<SocketCommandContext>
        {
            [Command("commandchannel")]
            public async Task AddCommandChannel(IGuildChannel channel)
            {
                if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
                {
                    if (Globals.UserHasPermission(Context, Globals.RequiredRole.Admin))
                    {
                        Guild guild = Globals.CurrentGuild(Context);
                        string result = "";
                        Globals.CurrentConfig.Guilds.Remove(guild);
                        if (guild.CommandChannels.Add(channel.Id))
                        {
                            result = $"Added {channel.Name} to command channels. I will now watch this channel for commands.";
                        }
                        else
                        {
                            result = $"Could not add {channel.Name} because I'm already watching it.";
                        }
                        Globals.CurrentConfig.Guilds.Add(guild);
                        Globals.CurrentConfig.WriteChanges();
                        await ReplyAsync(result);
                    }
                }
            }
        }
        [Group("remove")]
        public class Remove : ModuleBase<SocketCommandContext>
        {
            [Command("commandchannel")]
            public async Task RemoveCommandChannel(IGuildChannel channel)
            {
                if (Context.User.Id == Context.Guild.OwnerId || Globals.CurrentGuild(Context).CommandChannels.Contains(Context.Channel.Id))
                {
                    if (Globals.UserHasPermission(Context, Globals.RequiredRole.Admin))
                    {
                        Guild guild = Globals.CurrentGuild(Context);
                        string result = "";
                        Globals.CurrentConfig.Guilds.Remove(guild);
                        if (guild.CommandChannels.Remove(channel.Id))
                        {
                            result = $"Removed {channel.Name} from command channels. I will no longer watch this channel for commands.";
                        }
                        else
                        {
                            result = $"Could not remove {channel.Name} because it's not being watched.";
                        }
                        Globals.CurrentConfig.Guilds.Add(guild);
                        Globals.CurrentConfig.WriteChanges();
                        await ReplyAsync(result);
                    }
                }
            }
        }
    }
}
