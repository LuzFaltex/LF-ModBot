﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LFModBot.Commands;
using LFModBot.Settings;
using LFModBot.Utilities;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LFModBot
{
    class Program
    {
        static void Main() => new Program().MainAsync().GetAwaiter().GetResult();

        private CommandService _commands;
        private DiscordSocketClient _client;
        private IServiceProvider _services;
        private BotConfig _config;

        private CommandManager commandManager;


        public async Task MainAsync()
        {
            // Initialize all the things!
            _client = new DiscordSocketClient();
            _commands = new CommandService();

            _client.Log += Log;
            _client.JoinedGuild += JoinedGuild;
            _client.UserJoined += UserJoined;

            _services = new ServiceCollection()
                .AddSingleton(_client)
                .AddSingleton(_commands)
                .BuildServiceProvider();

            commandManager = new CommandManager();

            _config = JsonConvert.DeserializeObject<BotConfig>(File.ReadAllText("./Settings/config.json"), new MicrosecondEpochConverter());

            Globals.CurrentConfig = _config;

            Globals.MetaData = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);

            // Start it up
            await PrintHeaders();

            await CheckConfig(_config);

            await commandManager.InstallCommandsAsync(_client, _commands, _services, _config);

            await _client.LoginAsync(TokenType.Bot, _config.Token);
            await _client.StartAsync();

            // Block until the program is closed
            await Task.Delay(-1);
        }

        private async Task UserJoined(SocketGuildUser arg)
        {
            Guild guild = Globals.CurrentConfig.GetGuild(arg.Guild.Id);

            if (guild != null && guild.ModEvents.Count != 0)
            {
                ModEvent me = guild.ModEvents.Where(x => x.Type == "Bonk").FirstOrDefault(y => y.UserID == arg.Id);
                if (me != null)
                {
                    Globals.CurrentConfig.Guilds.Remove(guild);
                    guild.ModEvents.Remove(me);
                    me.Type = "Ban";
                    me.Reason = $"{me.Reason} (Contract fulfilled)";
                    guild.ModEvents.Add(me);
                    Globals.CurrentConfig.Guilds.Add(guild);
                    Globals.CurrentConfig.WriteChanges();

                    EmbedBuilder errorEmbed = new EmbedBuilder
                    {
                        Color = Color.Red,
                        Title = "🔨",
                        Description = me.ToString()
                    };

                    await ((ITextChannel)arg.Guild.GetChannel(guild.ModLogChannel)).SendMessageAsync("", embed: errorEmbed.Build());
                    await arg.Guild.AddBanAsync(arg, 0, me.Reason, new RequestOptions() { AuditLogReason = me.Reason });
                }
            }
        }

        private Task PrintHeaders()
        {
            Console.WriteLine($"{Globals.MetaData.ProductName} version {Globals.MetaData.FileVersion}");
            Console.WriteLine($"Copyright {Globals.GetYear()} LuzFaltex, all rights reserved");
            Console.WriteLine();
            return Task.CompletedTask;
        }

        private Task CheckConfig(BotConfig config)
        {
            if (string.IsNullOrWhiteSpace(config.Token) || config.Token == "Paste your bot token here")
            {
                Console.WriteLine("[Config] Please paste your bot token below:");
                Console.Write("> ");
                config.Token = Console.ReadLine();
                config.WriteChanges();
            }
            if (config.Owner == ulong.MaxValue)
            {
                Console.WriteLine("[Config] Please paste your user ID below:");
                Console.Write("> ");
                string input = Console.ReadLine();
                while (!ulong.TryParse(input, out ulong output))
                {
                    Console.WriteLine("Invalid input. Please paste your user ID below:");
                    Console.Write("> ");
                    input = Console.ReadLine();
                    config.WriteChanges();
                }
            }
            return Task.CompletedTask;
        }

        

        #region events
        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }
        /// <summary>
        /// Called when the bot joins a new guild
        /// </summary>
        /// <param name="arg">The guild it joined</param>
        private Task JoinedGuild(SocketGuild arg)
        {
            Globals.RegisterGuild(arg);
            return Task.CompletedTask;
        }

        #endregion
    }
}
