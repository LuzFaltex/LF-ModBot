﻿using Discord;
using LFModBot.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace LFModBot
{
    [Serializable]
    public class ModEvent : IEqualityComparer<ModEvent>
    {
        public string Type { get; set; }
        public int CaseID { get; set; }
        public string UserUsername { get; set; }
        public string UserDiscriminator { get; set; }
        public ulong UserID { get; set; }
        public string Reason { get; set; }
        public string ModUsername { get; set; }
        public string ModDiscriminator { get; set; }
        public ulong ModID { get; set; }
        [JsonProperty(ItemConverterType = typeof(MicrosecondEpochConverter))]
        public DateTime FileDate {get; set;}  
        public ulong MessageID { get; set; }

        public ModEvent(string Type, int CaseID, string UserUsername, string UserDiscriminator, ulong UserID, string Reason, string ModUsername, string ModDiscriminator, ulong ModID, DateTime FileDate)
        {
            this.Type = Type;
            this.CaseID = CaseID;
            this.UserUsername = UserUsername;
            this.UserDiscriminator = UserDiscriminator;
            this.UserID = UserID;
            this.Reason = Reason;
            this.ModUsername = ModUsername;
            this.ModDiscriminator = ModDiscriminator;
            this.ModID = ModID;
            this.FileDate = FileDate;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"**{Type}** | Case {CaseID}");
            sb.AppendLine($"**User**: {UserUsername}#{UserDiscriminator} ({UserID})");
            sb.AppendLine($"**Reason**: {Reason}");
            sb.AppendLine($"**Responsible Moderator**: {ModUsername}#{ModDiscriminator} ({ModID})");
            return sb.ToString();
        }

        public ModEvent AddMessageID(ulong ID)
        {
            MessageID = ID;
            return this;
        }

        public bool Equals(ModEvent x, ModEvent y)
        {
            return x.CaseID == y.CaseID;
        }

        public int GetHashCode(ModEvent obj)
        {
            return obj.CaseID;
        }
    }
}
