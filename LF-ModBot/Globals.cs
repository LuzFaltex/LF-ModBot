﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LFModBot.Settings;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFModBot.Commands
{
    internal static class Globals
    {
        internal static void RegisterGuild(SocketGuild arg)
        {
            // Register the guild information
            Guild guild = new Guild
            {
                Name = arg.Name,
                ID = arg.Id,
                Owner = arg.Owner.Id,
                ModEvents = new HashSet<ModEvent>()
            };

            if (CurrentConfig.Guilds.Any(x => x.ID == arg.Id))
            {
                CurrentConfig.Guilds.RemoveAll(x => x.ID == arg.Id);
            }
            CurrentConfig.Guilds.Add(guild);
            CurrentConfig.WriteChanges();
        }

        internal static string GetYear()
        {
            return $"2017{((DateTime.Now.Year > 2017) ? $" - {DateTime.Now.Year}" : "")}";
        }

        internal static FileVersionInfo MetaData { get; set; }

        internal static BotConfig CurrentConfig { get; set; }

        internal static Guild CurrentGuild(SocketCommandContext context)
        {
            return CurrentConfig.GetGuild(context.Guild.Id);
        }

        internal static IRole GetRole(SocketCommandContext context, RequiredRole reqRole)
        {
            if (reqRole == RequiredRole.Admin)
            {
                var role = context.Guild.GetRole(CurrentGuild(context).AdminRole);
                if (role != null)
                {
                    return role;
                }
                else return null;
            }
            else //(reqRole == RequiredRole.Mod)
            {
                var role = context.Guild.GetRole(CurrentGuild(context).ModRole);
                if (role != null)
                {
                    return role;
                }
                else return null;
            }
        }

        internal static string GetRoleText(SocketCommandContext context, RequiredRole reqRole)
        {
            if (reqRole == RequiredRole.Admin)
            {
                var role = context.Guild.GetRole(CurrentGuild(context).AdminRole);
                if (role != null)
                {
                    return $"{role.Name} ({role.Id})";
                }
                else return "none";
            }
            else //(reqRole == RequiredRole.Mod)
            {
                var role = context.Guild.GetRole(CurrentGuild(context).ModRole);
                if (role != null)
                {
                    return $"{role.Name} ({role.Id})";
                }
                else return "none";
            }
        }

        internal static string GetUserText(IUser user)
        {
            return $"{user.Username}#{user.Discriminator} ({user.Id})";
        }

        internal static bool UserHasPermission(SocketCommandContext context, RequiredRole requiredRole)
        {
            if (context.User.Id == CurrentConfig.Owner)
            {
                return true;
            }

            if (requiredRole == RequiredRole.Admin)
            {
                SocketRole permRole = context.Guild.GetRole(CurrentConfig.GetGuild(context.Guild.Id).AdminRole);
                if (permRole != null)
                {
                    return permRole.Members.Contains(context.User);
                }
                else { return false; }
            }
            else if (requiredRole == RequiredRole.Mod)
            {
                SocketRole permRole = context.Guild.GetRole(CurrentConfig.GetGuild(context.Guild.Id).ModRole);
                if (permRole != null)
                {
                    return permRole.Members.Contains(context.User);
                }
                else { return false; }
            }
            else
            {
                return true;
            }
        }

        internal enum RequiredRole
        {
            Mod,
            Admin
        }
    }
}
