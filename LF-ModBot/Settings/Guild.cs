﻿using Discord;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace LFModBot.Settings
{
    [Serializable]
    public class Guild
    {
        public Guild(string name = "", ulong id = ulong.MaxValue, ulong owner = ulong.MaxValue, char prefix = '!', ulong adminRole = ulong.MaxValue, ulong modRole = ulong.MaxValue, ulong modLodChannel = ulong.MaxValue, HashSet<ulong> commandChannels = null, HashSet<ModEvent> modEvents = null)
        {
            Name = name;
            ID = id;
            Owner = owner;
            Prefix = prefix;
            AdminRole = adminRole;
            ModRole = modRole;
            ModLogChannel = modLodChannel;
            if (commandChannels != null)
            {
                CommandChannels = commandChannels;
            }
            else
            {
                CommandChannels = new HashSet<ulong>();
            }
            if (modEvents != null)
            {
                ModEvents = modEvents;
            }
            else
            {
                modEvents = new HashSet<ModEvent>();
            }
        }
        public string Name { get; set; }
        public ulong ID { get; set; }
        public ulong Owner { get; set; }
        [DefaultValue('!')]
        public char Prefix { get; set; }
        public ulong AdminRole { get; set; }
        public ulong ModRole { get; set; }
        public ulong ModLogChannel { get; set; }
        public HashSet<ulong> CommandChannels { get; set; }
        public HashSet<ModEvent> ModEvents { get; set; }
    }
}
