﻿using LFModBot.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFModBot.Settings
{
    [Serializable]
    public class BotConfig
    {
        /// <summary>
        /// Initialize and gather values.
        /// </summary>
        /// <exception cref="FileNotFoundException">Thrown if the config.json doesn't exist.</exception>
        public BotConfig(string Token, ulong Owner, List<Guild> Guilds)
        {
            this.Token = Token;
            this.Owner = Owner;
            this.Guilds = Guilds;
        }

        public Guild GetGuild(ulong GuildID)
        {
            try
            {
                return Guilds.First(x => x.ID == GuildID);
            }
            catch
            {
                return new Guild();
            }
        }

        public void WriteChanges()
        {
            JsonSerializer serializer = new JsonSerializer
            {
                Formatting = Formatting.Indented,
            };
            serializer.Converters.Add(new MicrosecondEpochConverter());

            using (StreamWriter sw = new StreamWriter("./Settings/config.json"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, this);
            }
        }

        public string Token { get; set; }
        [DefaultValue(ulong.MaxValue)]
        public ulong Owner { get; set; }
        public List<Guild> Guilds { get; set; }
    }
}
